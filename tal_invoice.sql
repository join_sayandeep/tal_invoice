-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 11, 2019 at 08:40 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.0.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tal_invoice`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `business_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gstin` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `phone`, `business_name`, `address`, `email`, `gstin`, `created_at`, `updated_at`) VALUES
(1, 'Sayandeep Majumdar', '8017730776', 'TAL', 'Jadavpur', 'smajumdar1993@gmail.com', 'NA', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Jack Smith', '7895425222', 'A1 Solution Pvt ltd', 'B22, Lake view, london-122', 'jack@gmail.com', 'AA1222287878FGT', '2019-06-09 02:12:13', '2019-06-09 02:12:13'),
(3, 'Sayandeep Majumdar', '8017730776', 'NA', 'B/13, Bapujinagar, Sulekha', 'NA', 'NA', '2019-06-09 02:13:58', '2019-06-09 02:13:58');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `invoice_number` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `description` varchar(800) NOT NULL,
  `sac` varchar(255) NOT NULL,
  `qty` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `amt` varchar(255) NOT NULL,
  `subtotal` varchar(255) NOT NULL,
  `due` varchar(255) NOT NULL,
  `igst` varchar(255) NOT NULL,
  `cgst` varchar(255) NOT NULL,
  `sgst` varchar(255) NOT NULL,
  `gst_amt` varchar(255) NOT NULL,
  `discount_amt` varchar(255) DEFAULT NULL,
  `discount_percentage` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `invoice_number`, `date`, `client_id`, `description`, `sac`, `qty`, `rate`, `amt`, `subtotal`, `due`, `igst`, `cgst`, `sgst`, `gst_amt`, `discount_amt`, `discount_percentage`, `created_at`, `updated_at`) VALUES
(1, 'TAL/79/09/1234', '2019-06-09', '1', '1. Website Development\r\n2. Blog', '123321', '1', '1000', '1000', '1000', '0', '0', '2.5', '2.5', '1050', '0', '0', '2019-06-09 05:36:41', '2019-06-11 11:49:56'),
(2, 'TAL/79/09/1222', '2019-06-22', '1', 'sjkndjksndnskjdsd', 'q1212d', '1', '1000', '500', '500', '558.4000000000001', '0', '6', '6', '1008', '100', '10', '2019-06-11 08:46:49', '2019-06-11 08:46:49'),
(3, 'TAL/79/09/1211', '2019-06-11', '1', 'sbdhjbsjdbsjdbhjsds', '12121112', '1', '1200', '1000', '1000', '1514.1361895014402', '0', '2.5', '2.5', '1071', '180', '15', '2019-06-11 08:47:30', '2019-06-11 08:47:30'),
(4, 'TAL/79/09/0000', '2019-06-13', '1', ',ncxmc m,xncm,ncxc', 'mkmakakj', '2', '1200', '1000', '1000', '1520', '0', '2.5', '2.5', '2520', '0', '0', '2019-06-11 08:48:03', '2019-06-11 08:48:03'),
(5, 'TAL/79/09/999', '2019-06-13', '2', 'sdjkksjdbkjsdkjsd', '123123', '1', '1000', '100', '100', '950', '0', '2.5', '2.5', '1050', '0', '0', '2019-06-11 08:48:36', '2019-06-11 08:48:36'),
(6, 'TAL/79/09/5220', '2019-06-11', '1', 'sjdmsnmsd', 'hwhkh12', '1', '12333', '1500', '1500', '11449.65', '5', '0', '0', '12949.65', '0', '0', '2019-06-11 08:49:13', '2019-06-11 08:49:13'),
(7, 'TAL/79/09/7539', '2019-06-11', '1', 'sdnmsndmnsmdsd', '12121', '1', '15000', '100', '100', '15650', '5', '0', '0', '15750', '0', '0', '2019-06-11 08:49:49', '2019-06-11 08:49:49'),
(8, 'TAL/79/09/12344522', '2019-06-04', '1', 'sndnmsdmsmd', '1231212', '12', '500', '1000', '1000', '5300', '0', '2.5', '2.5', '6300', '0', '0', '2019-06-11 08:50:27', '2019-06-11 08:50:27'),
(9, 'TAL/79/09/123222', '2019-06-11', '1', 'jksndksnkdjsdsd', '121333', '1', '1000', '1000', '1000', '29', '0', '2.5', '2.5', '1029', '20', '2', '2019-06-11 08:51:03', '2019-06-11 08:51:03'),
(10, 'TAL/79/09/123111', '2019-06-06', '1', 'skndksnjksnjdsd', '1231212', '1', '12000', '1000', '1000', '1898', '0', '2.5', '2.5', '2898', '9240', '77', '2019-06-11 08:51:35', '2019-06-11 08:51:35'),
(11, 'TAL/79/09/1230000', '2019-06-04', '1', 'skdjsknkdjsd', '12121', '1', '1000', '1000', '1000', '50', '5', '0', '0', '1050', '0', '0', '2019-06-11 08:52:27', '2019-06-11 08:52:27'),
(12, 'TAL/79/09/10200', '2019-06-11', '1', 'web App', '1231212', '1', '2000', '1000', '1000', '890.00', '0', '2.5', '2.5', '1890', '200', '10', '2019-06-11 12:05:39', '2019-06-11 12:05:39'),
(13, 'TAL/79/09/12340a', '2019-06-06', '3', 'kjshdkjsd', 'jkndkjn', '1', '1000', '1050', '1050', '0.00', '0', '2.5', '2.5', '1050', '0', '0', '2019-06-11 12:10:00', '2019-06-11 12:10:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_05_20_100836_create_posts_table', 1),
(3, '2019_05_20_102034_create_comments_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `user_name`, `user_password`, `user_email`, `user_status`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'Sayandeep Majumdar', 'sayan@gmail.com', '$2y$10$V1lz0HUQvoFWNa0x4U3Jv.vyWe.Vxb3zTB7maHly5YOFyW/Onwc3q', 'sayan@gmail.com', 1, '2019-05-24 15:15:21', '2019-05-24 15:15:21', NULL),
(3, 'Sayandeep Majumdar', 'dd@gmail.com', '$2y$10$TIGTsUnYIg.Pg5tbV/nopORTNlcqjVafUqF57ImueUssCSeV8RTu6', 'dd@gmail.com', 1, '2019-05-24 15:45:14', '2019-05-24 15:45:14', NULL),
(4, 'Sayandeep Majumdar', 'cc@gmail.com', '$2y$10$Q3et3WfWyXM9p6kFp5sBi.Nnw7wQWU38pXVHM4a6xLLqzmVRM5r6q', 'cc@gmail.com', 1, '2019-05-24 15:45:53', '2019-05-24 15:45:53', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
