<?php

namespace App\Http\Controllers\tal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Customer;


class CustomerController extends Controller
{
	public function CustomerEntryService(Request $req)
	{
		$name = $req->input('name');
		$business_name = $req->input('business_name');
		$ph = $req->input('ph');
		$address = $req->input('address');
		$email = $req->input('email');
		$gstin = $req->input('gstin');

		$timenow = Carbon::now(); 

		  if(empty($email) || empty($gstin) ) {  
		       
		        $user = new Customer();
		        $user->name = $name;
		        $user->phone = $ph;
		        $user->business_name = $business_name;
		        $user->address = $address;
		        $user->email = "NA";
		        $user->gstin = "NA";
		        $user->created_at = $timenow;
		        $user->updated_at = $timenow;
		        $user->save();

		        \Session::flash('message', "Customer Added Successfully"); 
				\Session::flash('alert-class', 'alert-success'); 

		        return redirect()->back();
		    }else{

		    	$user = new Customer();
		        $user->name = $name;
		        $user->phone = $ph;
		        $user->business_name = $business_name;
		        $user->address = $address;
		        $user->email = $email;
		        $user->gstin = $gstin;
		        $user->created_at = $timenow;
		        $user->updated_at = $timenow;
		        $user->save();

		        \Session::flash('message', "Customer Added Successfully"); 
				\Session::flash('alert-class', 'alert-success'); 

		        return redirect()->back();
		    }

	}
    
}
