<?php

namespace App\Http\Controllers\tal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Customer;
use App\Quotation;


class QuotationController extends Controller
{
	public function showQuotationPage()
	{
		return view("tal.pages.quotation.quotation");

	}

	public function saveQuotation(Request $req)
	{
		$customer_id = $req->input('customer_id');
        
        $dt = $req->input('dt');
        $q_no = $req->input('q_no');
        $qty = $req->input('qty');
        $rate = $req->input('rate');
        $discount = $req->input('discount');
        
        $gst_type = $req->input('gst_type');
        $gst_percentage = $req->input('gst_percentage');
        $amt = $req->input('amt');

        $subtotal = $req->input('subtotal');
		$description = $req->input('description');
		
		$created_time = Carbon::now(); 
		$quotation_check = Quotation::where('quotation_no',$q_no)->first();

		if(empty($quotation_check)){
			$quotation = new Quotation();

			$quotation->customer_id  = $customer_id;
			$quotation->date  = $dt;
			$quotation->description  = $description;
			$quotation->quotation_no  = $q_no;
			$quotation->qty  = $qty;
			$quotation->rate  = $rate;
			$quotation->amt  = $amt;
			$quotation->subtotal  = $subtotal;

		   if ($gst_type == "0") {
				$quotation->cgst  = $gst_percentage / 2;
				$quotation->sgst  = $gst_percentage / 2;
				$quotation->igst  = '0';
		   }else{
				$quotation->igst  = $gst_percentage;
				$quotation->cgst  = '0';
				$quotation->sgst  = '0';
		   }
			
			$quotation->discount  = $discount;
			$quotation->gst_type  = $gst_type;
			$quotation->gst_percentage  = $gst_percentage;
			$quotation->created_at  = $created_time;
			$quotation->updated_at  = $created_time;

			$quotation->save();


	   \Session::flash('message', "Quotation has been created"); 
	   \Session::flash('alert-class', 'alert-success'); 
				   return redirect()->back(); 

		}else{
			\Session::flash('message', "Quotation Number should be Unique"); 
            \Session::flash('alert-class', 'alert-danger'); 
                        return redirect()->back();
		}
	}

	public function showQuotationList()
	{
		$quotation_data = Quotation::orderBy('created_at', 'desc')->paginate(10);

        return view('tal.pages.quotation.quotationlist',compact('quotation_data'));
	}
    
}
