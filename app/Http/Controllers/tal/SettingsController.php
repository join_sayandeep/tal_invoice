<?php

namespace App\Http\Controllers\tal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Invoice;
use App\Users;
Use PDF;

class SettingsController extends Controller
{
     public function showSettingPage()
     {
         return view('tal.pages.settings');
     }

}