<?php

namespace App\Http\Controllers\tal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Invoice;
use App\Users;
Use PDF;

class InvoiceController extends Controller
{
    public function saveInvoice(Request $req)
    {
    	$customer_id = $req->input('customer_id');
        
        $invoice_dt = $req->input('invoice_dt');
        $invoice_no = $req->input('invoice_no');
        $sac = $req->input('sac');

        $qty = $req->input('qty');
        $rate = $req->input('rate');
        $discount = $req->input('discount');
        
        $gst_type = $req->input('gst_type');
        $gst_percentage = $req->input('gst_percentage');
        $amt = $req->input('amt');

        $subtotal = $req->input('subtotal');
        $due = $req->input('due');
        $description = $req->input('description');

        $total_amt = $qty * $rate;
        $amt_discounted = ($total_amt * $discount)/100;

        $after_discount_amt = $total_amt - $amt_discounted;

        $gst_amt = ($after_discount_amt * $gst_percentage) / 100;

        $after_gst_amt = $after_discount_amt + $gst_amt;

        $created_time = Carbon::now(); 

        $invoice_copy_checking = Invoice::where('invoice_number',$invoice_no)->first();

        if(empty($invoice_copy_checking)){
                 $invoices = new Invoice();

                 $invoices->invoice_number  = $invoice_no;
                 $invoices->date  = $invoice_dt;
                 $invoices->client_id  = $customer_id;
                 $invoices->description  = $description;
                 $invoices->sac   = $sac;
                 $invoices->qty  = $qty;
                 $invoices->rate  = $rate;
                 $invoices->amt  = $amt;
                 $invoices->subtotal  = $subtotal;
                 $invoices->due  = $due;

                if ($gst_type == "0") {
                     $invoices->cgst  = $gst_percentage / 2;
                     $invoices->sgst  = $gst_percentage / 2;
                     $invoices->igst  = '0';
                }else{
                     $invoices->igst  = $gst_percentage;
                     $invoices->cgst  = '0';
                     $invoices->sgst  = '0';
                }
                 
                

                 $invoices->gst_amt  = $after_gst_amt;
                 $invoices->discount_amt  = $amt_discounted;
                 $invoices->discount_percentage  = $discount;
                 $invoices->created_at  = $created_time;
                 $invoices->updated_at  = $created_time;

                 $invoices->save();


            \Session::flash('message', "Invoice has been created"); 
            \Session::flash('alert-class', 'alert-success'); 
                        return redirect()->back();  
        }else{
            \Session::flash('message', "Invoice Number should be Unique"); 
            \Session::flash('alert-class', 'alert-danger'); 
                        return redirect()->back();  
        }
    }


    public function showInvoiceList()
    {
        $invoice_data = Invoice::orderBy('created_at', 'desc')->paginate(10);

        return view('tal.pages.invoice.invoicelist',compact('invoice_data'));
    }

    

    
}
