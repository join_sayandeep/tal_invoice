<?php

namespace App\Http\Controllers\tal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Invoice;
use App\Users;
Use PDF;

class PDFConversionController extends Controller
{
    public function getInvoiceInfo($id)
    {
        $invoice_data = Invoice::where('id',$id)->first();

        return $invoice_data;
    }

    public function PDFToInvoiceConversion($id)
    {
        $pdfengine = \App::make('dompdf.wrapper');
        $data_info = $this->getInvoiceInfo($id);
        
        // $pdfengine->loadHTML($this->dataToHtml($id));
        $data = ['id' => $data_info->invoice_number];
        $pdfengine->loadView('tal.pages.invoice.invoicePdfView', $data);

        return $pdfengine->stream();

        // return $pdfengine->download('invoice.pdf');
    }

    public function PDFToQuotationConversion($id)
    {
        $pdfengine = \App::make('dompdf.wrapper');
        $data_info = $this->getInvoiceInfo($id);
        
        // $pdfengine->loadHTML($this->dataToHtml($id));
        $data = ['id' => $data_info->invoice_number];
        $pdfengine->loadView('tal.pages.quotation.quotationPdfView', $data);

        return $pdfengine->stream();

        // return $pdfengine->download('invoice.pdf');
    }

}
