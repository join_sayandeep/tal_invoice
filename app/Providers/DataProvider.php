<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Customer;

class DataProvider extends ServiceProvider
{
     public static function customers_list()
    {
         $customer_list =  Customer::all();

               
          return $customer_list;
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
