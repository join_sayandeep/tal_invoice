-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 29, 2019 at 01:20 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.0.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trastaven`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `comment_post_id` bigint(20) NOT NULL,
  `comment_author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_date` datetime NOT NULL,
  `comment_content` varchar(999) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_05_20_100836_create_posts_table', 1),
(3, '2019_05_20_102034_create_comments_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) NOT NULL,
  `post_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_featured_img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` varchar(155) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_content` varchar(999) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_date` datetime NOT NULL,
  `comment_status` int(11) NOT NULL,
  `comment_count` int(11) NOT NULL,
  `post_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categories` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `post_author`, `post_title`, `post_featured_img`, `post_excerpt`, `post_status`, `post_content`, `post_date`, `comment_status`, `comment_count`, `post_slug`, `categories`, `tags`, `created_at`, `updated_at`) VALUES
(1, 1, 'h m', 'h m.jpg', '<p>asasas</p>', 'draft', '<h2 style=\"text-align: center;\">Superman is Hope for World.</h2>\r\n<p>I am a big fan of Superman. It\'s a Dark Comic Universe Character. It has a tremendous strong ability to defeat any other villain or any characters.&nbsp;</p>\r\n<p>&nbsp;</p>', '2019-05-24 16:39:15', 0, 0, 'h-m', 'jksdkjsd', 'ddd', '2019-05-24 16:39:15', '2019-05-24 16:39:15'),
(2, 1, 'i am', 'i am.jpg', '<p>asas</p>', 'published', '<figure class=\"image align-left\"><img src=\"http://www.thinkagainlab.com/bower_components/AdminLTE/dist/img/logotal.png\" alt=\"hi\" width=\"100\" height=\"41\" />\r\n<figcaption>Caption</figcaption>\r\n</figure>\r\n<p>&nbsp; nsaajknjksdn</p>\r\n<p>&nbsp;</p>', '2019-05-25 13:34:57', 0, 0, 'i-am', 'a', 's', '2019-05-25 13:34:57', '2019-05-25 13:34:57'),
(3, 1, 'sbjshdjkhsd df', 'sbjshdjkhsd df.jpg', 'snd,mns,dmsd', 'published', '<p>msndnsmdnmsnd&nbsp;<strong>skdlskdlksdlksd</strong></p>', '2019-05-29 15:34:58', 0, 0, 'sbjshdjkhsd-df', 'jksdkjsd', 'sd', '2019-05-29 15:34:58', '2019-05-29 15:34:58'),
(4, 1, 'jkdjjdd djkdk', 'jkdjjdd djkdk.jpg', 'nmnd', 'published', '<p>jdnkjdnkjdn&nbsp;<strong>kslms</strong></p>', '2019-05-29 15:37:25', 0, 0, 'jkdjjdd-djkdk', 's,s', 's', '2019-05-29 15:37:25', '2019-05-29 15:37:25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `user_name`, `user_password`, `user_email`, `user_status`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'Sayandeep Majumdar', 'sayan@gmail.com', '$2y$10$V1lz0HUQvoFWNa0x4U3Jv.vyWe.Vxb3zTB7maHly5YOFyW/Onwc3q', 'sayan@gmail.com', 1, '2019-05-24 15:15:21', '2019-05-24 15:15:21', NULL),
(3, 'Sayandeep Majumdar', 'dd@gmail.com', '$2y$10$TIGTsUnYIg.Pg5tbV/nopORTNlcqjVafUqF57ImueUssCSeV8RTu6', 'dd@gmail.com', 1, '2019-05-24 15:45:14', '2019-05-24 15:45:14', NULL),
(4, 'Sayandeep Majumdar', 'cc@gmail.com', '$2y$10$Q3et3WfWyXM9p6kFp5sBi.Nnw7wQWU38pXVHM4a6xLLqzmVRM5r6q', 'cc@gmail.com', 1, '2019-05-24 15:45:53', '2019-05-24 15:45:53', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
