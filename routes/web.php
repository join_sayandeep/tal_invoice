<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login', function () {
    return view('tal.pages.login');
});
Route::get('/', function () {
    return view('tal.pages.login');
});

Route::post('/login/service','tal\AccountController@loginService');

Route::get('/create/account', function () {
    return view('tal.pages.registration');
});
Route::post('/create/account/service','tal\AccountController@signupService');


Route::group(['middleware' => 'validatesession'],function(){   //Session validation in every pages



Route::post('/customer/save','tal\CustomerController@CustomerEntryService');

Route::prefix('invoice')->group(function () {

Route::get('/', function () {
        return view('tal.pages.invoice.invoice');
    });
Route::post('/save','tal\InvoiceController@saveInvoice');
Route::get('/list','tal\InvoiceController@showInvoiceList');
Route::get('/pdf/{id}','tal\PDFConversionController@PDFToInvoiceConversion');

});


Route::prefix('quotation')->group(function () {

Route::get('/','tal\QuotationController@showQuotationPage');
Route::post('/save','tal\QuotationController@saveQuotation');
Route::get('/list','tal\QuotationController@showQuotationList');
Route::get('/pdf/{id}','tal\PDFConversionController@PDFToQuotationConversion');

});

Route::get('/settings','tal\SettingsController@showSettingPage');
Route::get('/logout','tal\AccountController@logout');
});


// Route::get('/tal/del', function (){

// exec('del "D:\Test_1\Test\*.*" /s /f /q');

// });