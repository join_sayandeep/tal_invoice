@extends('tal.layouts.quotationlayout')

@section('content')

<!-- Page content -->
<form action="/quotation/save" method="post" enctype="multipart/form-data">
  <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
    <div class="container-fluid mt--7">
    @if(Session::has('message'))
              <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
              @endif
     <!--  <div class="row">
 
        <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
          <div class="card card-profile shadow">
            <div class="row justify-content-center">
              <div class="col-lg-3 order-lg-2">
                <div class="card-profile-image">
                  <a href="#"> -->
                    <!-- <img src="{{ asset('trastaven/img/theme/team-4-800x800.jpg') }}" class="rounded-circle"> -->
                 <!--  </a>
                </div>
              </div>
            </div>
            <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
              <div class="d-flex justify-content-between">
               

              </div>
            </div>
            <div class="card-body pt-0 pt-md-4"> -->
              <!-- <input type="hidden" name="date" value="@php  $mytime = Carbon\Carbon::now(); 
                     echo $mytime; @endphp"> -->
              <!-- <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">Rate</label>
                        <input id="input-address" name="rate" class="form-control form-control-alternative" placeholder="Rate"  type="text" required>
                     </div>
                    </div>
                  </div>
                </div>
              </div> -->
              <!-- <hr class="my-4"> -->
             <!--  <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">Amount</label>
                        <input id="input-address" name="amt" class="form-control form-control-alternative" placeholder="Amount"  type="text" required>
                      </div>
                    </div>
                  </div>
                </div>
              </div> -->
              <!-- <hr class="my-4"> -->
              <!-- <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">Sub Total</label>
                        <input id="input-address" name="subtotal" class="form-control form-control-alternative" placeholder="Sub Total"  type="text" required>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">Due</label>
                        <input id="input-address" name="due" class="form-control form-control-alternative" placeholder="Due"  type="text" required>
                     </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">GST Type</label>
                        <select id="input-address" name="gst_type" class="form-control form-control-alternative" required>
                        <option value="0">Intra-State</option>
                        <option value="1">Inter-State</option>
                        </select>
                       </div>
                    </div>
                  </div>
                </div>
              </div> -->
              <!-- <hr class="my-4"> -->
              <!-- <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                    

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> -->
        <div class="col-xl-12 order-xl-1" id='app'>
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Create a quotation</h3>
                </div>
                <div class="col-4 text-right">
                  <a href="/quotation/list" class="btn btn-sm btn-primary">See Lists</a>
                </div>
              </div>
            </div>
            <div class="card-body">
                <!-- <h6 class="heading-small text-muted mb-4">User information</h6> -->
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Quote to</label>
                        <select id="input-address" name="customer_id" class="form-control form-control-alternative" required>
                        @foreach(Info::customers_list() as $customer)
                        <option value="{{ $customer->id }}">{{ $customer->name }} [{{ $customer->business_name }}]</option>
                        @endforeach
                        </select>
                      </div>
                    </div>
                   
                    <div class="col-lg-6">
                      <div class="form-group">
                        
                        <button type="button" data-toggle="modal" data-target="#VisitorRecord" class="btn btn-sm btn-info mr-4"><i class="fa fa-plus" aria-hidden="true"></i></button>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Date</label>
                        <input id="input-file" name="dt" class="form-control form-control-alternative" placeholder="Quotation Date"  type="date" required>
                      </div>
                    </div>
                   
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Quotation #</label>
                        <input type="text" id="input-email" name="q_no" class="form-control form-control-alternative" placeholder="Quotation Number">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    
                    <div class="col-lg-6">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">Quantity</label>
                        <input id="input-qty" name="qty" class="form-control form-control-alternative" placeholder="Qty"  type="text" required onchange="cal(this)">
                     </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">Rate</label>
                        <input id="input-rate"  name="rate" class="form-control form-control-alternative" placeholder="Rate"  type="text" required onchange="cal(this)">
                     </div>
                    </div>
                  </div>
                </div>
                <!-- <hr class="my-4" /> -->
                <!-- Address -->
                <!-- <h6 class="heading-small text-muted mb-4">Contact information</h6> -->
                <div class="pl-lg-4">
                  <div class="row">
                    
                    <div class="col-md-6">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">Discount</label>
                        <input id="input-discount" name="discount" class="form-control form-control-alternative" placeholder="discount %"  type="text" required onchange="caldisc()">
                     </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Amount</label>
                        <input id="input-amt" id="amt" name="amt" class="form-control form-control-alternative" placeholder="Amount"  type="text" onchange="caldue()">
                       
                      </div>
                  </div>
                  </div>
                  <div class="row">
                  <div class="col-lg-4">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">GST Type</label>
                        <select id="input-address" name="gst_type" class="form-control form-control-alternative" required>
                        <option value="0">Intra-State</option>
                        <option value="1">Inter-State</option>
                        </select>
                         </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">GST</label>
                        <select id="input-gst" name="gst_percentage" class="form-control form-control-alternative" required onchange="calgst()">
                        <option value="0">0%</option>
                        <option value="5">5%</option>
                        <option value="12">12%</option>
                        <option value="18">18%</option>
                        <option value="28">28%</option>
                        </select> </div>
                    </div>
                    
                  <div class="col-lg-4">
                      <div class="form-group">
                       <label class="form-control-label" for="input-address">Sub Total</label>
                        <input id="input-subtotal" id="subtotal" name="subtotal" class="form-control form-control-alternative" placeholder="Sub Total"  type="text" readonly>
                       

                       </div>
                  <div class="row">
                    
                    </div>
                    
                    </div>
                   
<script type="text/javascript">
  function cal() {
    
    var qty= document.getElementById("input-qty").value;
    var rate= document.getElementById("input-rate").value;

    var amt = (qty*rate).toFixed(2);

    document.getElementById('input-subtotal').value = amt;
    document.getElementById('input-amt').value = amt;
   
  }

  function caldisc() {
    var discount = document.getElementById("input-discount").value;
    var rate= document.getElementById("input-rate").value;
    var qty= document.getElementById("input-qty").value;
    if(discount == "0"){
   
      document.getElementById("input-subtotal").value = (rate*qty).toFixed(2);
    }else{
    var amt = (rate * discount)/100;
    var subtotal = (rate - amt).toFixed(2);
    document.getElementById("input-subtotal").value = subtotal;
    document.getElementById("input-amt").value = subtotal;
    }

    
  }
  function caldue(){
    
    var amt = document.getElementById("input-amt").value;
    var subtotal= document.getElementById("input-subtotal").value;

    document.getElementById("input-subtotal").value = amt;
  }
  function calgst() {
    var gst = document.getElementById("input-gst").value;
    var subtotal= document.getElementById("input-subtotal").value;
    var val = subtotal;
    var gstval = ((val * gst) /100).toFixed(2);

    document.getElementById("input-subtotal").value = Number(val) + Number(gstval);
    document.getElementById("input-discount").readOnly = true;

  }
  
</script>

                    
                  </div>
                </div>
                <hr class="my-4" />
                <!-- Description -->
                <!-- <h6 class="heading-small text-muted mb-4">About me</h6> -->
                <div class="pl-lg-4">

                  <div class="form-group">
                  <label class="form-control-label" for="input-country">Description</label>
                    <textarea rows="20" name="description" id="editor1" class="form-control form-control-alternative" placeholder="Description ..." ></textarea>
                      </div>
                </div>

                <button type="submit" name="submit_type" value="published" class="btn btn-sm btn-default float-right">Save</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      @include('tal.pages.quotation.customerModal')

@endsection