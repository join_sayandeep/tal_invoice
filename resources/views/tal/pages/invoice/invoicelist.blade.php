@extends('tal.layouts.invoicelayout')

@section('content')
<div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">Lists of Invoices</h3>
              <div class="text-right">
                  <a href="/invoice" class="btn btn-sm btn-primary">Back</a>
                </div>
            </div>
            
                 
                
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Invoice#</th>
                    <th scope="col">Date</th>
                    <th scope="col">Paid</th>
                    <th scope="col">Description</th>
                    <th scope="col">Amt(&#8377;)</th>
                    <th scope="col">Due(&#8377;)</th>
                    <th scope="col">Discount %</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                @foreach($invoice_data as $data)
                  <tr>
                    <th scope="row">
                      <!-- <div class="media align-items-center">
                        <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="../assets/img/theme/bootstrap.jpg">
                        </a> -->
                        <div class="media-body">
                         <a href="#" data-toggle="modal" data-target="#VisitorRecord" class="button"> <span class="mb-0 text-sm">{{ $data->invoice_number }}</span></a>
                         @include('tal.pages.invoice.customerModal')
                        </div>
                      <!-- </div> -->
                    </th>
                    <td>
                    {{ $data->date }}
                    </td>
                    <td>
                      <span class="badge badge-dot mr-4">
                      @if($data->due != 0)
                      <i class="bg-warning"></i>  <a href="#"> {{ $data->client_id }} </a>
                      @else
                      <i class="bg-success"></i>  <a href="#"> {{ $data->client_id }} </a>
                      @endif
                      </span>
                    </td>
                    <td>
                      <div class="avatar-group">
                      {{ $data->description }}
                        <!-- <a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="Ryan Tompson">
                          <img alt="Image placeholder" src="../assets/img/theme/team-1-800x800.jpg" class="rounded-circle">
                        </a>
                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="Romina Hadid">
                          <img alt="Image placeholder" src="../assets/img/theme/team-2-800x800.jpg" class="rounded-circle">
                        </a>
                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="Alexander Smith">
                          <img alt="Image placeholder" src="../assets/img/theme/team-3-800x800.jpg" class="rounded-circle">
                        </a>
                        <a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="Jessica Doe">
                          <img alt="Image placeholder" src="../assets/img/theme/team-4-800x800.jpg" class="rounded-circle">
                        </a> -->
                      </div>
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">&#8377; {{ $data->amt }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">&#8377; {{ $data->due }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">{{ $data->discount_percentage }} %</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>
                    <td class="text-right">
                      <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                          <a class="dropdown-item" href="/invoice/pdf/{{ $data->id }}"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Download PDF</a>
                          <a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i>Send in Mail</a>
                          <!-- <a class="dropdown-item" href="#"></a> -->
                        </div>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            {{ $invoice_data->render("pagination::bootstrap-4") }}
           
          </div>
        </div>
      </div>
     



@endsection