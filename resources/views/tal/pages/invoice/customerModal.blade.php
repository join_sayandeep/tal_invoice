<div class="modal" id="VisitorRecord" 
     tabindex="-1" role="dialog" 
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">        
        <h4 class="modal-title" 
        id="favoritesModalLabel">Add Customer's Records </h4>
        <button type="button" class="close" 
          data-dismiss="modal"
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
      </div>
   
      <div class="modal-body">
      @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    <form action="/customer/save" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="body">                            
                            <div class="pl-lg-4">
                              <div class="row">
                                <div class="col-lg-6">
                                  <div class="form-group">
                                    <label class="form-control-label" for="input-username">Name</label>
                                    <input id="input-file" name="name" class="form-control form-control-alternative" placeholder="Customer's Name"  type="text" required>
                                  </div>
                                </div>
                                <div class="col-lg-6">
                                  <div class="form-group">
                                    <label class="form-control-label" for="input-username">Business Name</label>
                                    <input id="input-file" name="business_name" class="form-control form-control-alternative" placeholder="Business Name"  type="text" required>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="col-lg-6">
                                  <div class="form-group">
                                    <label class="form-control-label" for="input-username">Phone#</label>
                                    <input id="input-file" name="ph" class="form-control form-control-alternative" placeholder="Phone Number" maxlength="10"  type="text" required>
                                  </div>
                                </div>
                                <div class="col-lg-6">
                                  <div class="form-group">
                                    <label class="form-control-label" for="input-username">Address</label>
                                    <input id="input-file" name="address" class="form-control form-control-alternative" placeholder="Address"  type="text" required>
                                  </div>
                                </div>
                              </div>

                              <div class="row" style="align-items: right;">
                                <div class="col-lg-6">
                                  <div class="form-group">
                                    <label class="form-control-label" for="input-username">Email-Id</label>
                                    <input id="input-file" name="email" class="form-control form-control-alternative" placeholder="Email-Id (if any)"  type="email">
                                  </div>
                                </div>
                                <div class="col-lg-6">
                                  <div class="form-group">
                                    <label class="form-control-label" for="input-username">GSTIN</label>
                                    <input id="input-file" name="gstin" class="form-control form-control-alternative" placeholder="GSTIN (if any)" maxlength="15"  type="text">
                                  </div>
                                </div>
                              </div>
                                 <div class="row">
                                <div class="col-lg-12">                                 
                                 <button type="reset" class="btn btn-primary m-t-15 waves-effect">Reset</button>
                                 <button type="submit" class="btn btn-primary m-t-15 waves-effect">Save</button>
                                </div>
                              </div>
                            </div>
                       
                           
                            
                        </div>
                    </form>
      </div>
    </div>
  </div>
</div>