<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Think Again Lab">
  <title>Invoice Dashboard</title>
  <!-- Favicon -->
  <link href="{{ asset('tal/img/brand/favicon.png') }}" rel="icon" type="image/png">
  <!--  <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=qp8siq6adbd9fafuuylzehh8w773yre90kkfaa27w9xhdl43"></script> -->
  <!-- <script>tinymce.init({selector:'textarea'});</script> -->
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="{{ asset('tal/vendor/nucleo/css/nucleo.css') }}" rel="stylesheet">
  <link href="{{ asset('tal/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="{{ asset('tal/css/argon.css?v=1.0.0') }}" rel="stylesheet">
  <link type="text/css" href="{{ asset('tal/css/custom.css') }}" rel="stylesheet">

  <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=qp8siq6adbd9fafuuylzehh8w773yre90kkfaa27w9xhdl43"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  
</head>