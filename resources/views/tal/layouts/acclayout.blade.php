 <!DOCTYPE html>
<html>
 @include('tal.include.headerlinks')
<body class="bg-default">

 @yield('content')

 @include('tal.include.loginfooter')
 @include('tal.include.footerlinks')
</body>

</html>